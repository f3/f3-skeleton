<?php

require __DIR__.'/lib/base.php';

// F3::set('CACHE',TRUE);
F3::set('DEBUG',3);
F3::set('UI','app/views/');
F3::set('AUTOLOAD','app/');

require __DIR__.'/data/config.php';

F3::set('APP_NAME', 'F3 test');

F3::route('GET /@controller/@action','controllers\{{@PARAMS.controller}}->{{@PARAMS.action}}');
F3::route('GET /@controller','controllers\{{@PARAMS.controller}}->index');
F3::route('GET /', 'controllers\main->index');

F3::run();
