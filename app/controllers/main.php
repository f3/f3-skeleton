<?php
namespace controllers;

class main {
  function index() {
    \F3::set('PageTitle', 'Inicio');
    $table = new \Axon('table1');
    \F3::set('data', $table->aselect());
    echo \Template::serve('main/index.html');
  }

  function another() {
    \F3::set('PageTitle', 'Another page');
    echo \Template::serve('main/another.html');
  }
}
